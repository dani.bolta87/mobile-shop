/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { FontAwesome } from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as React from "react";
import { useEffect, useState } from "react";
import { ColorSchemeName, Pressable } from "react-native";

import Colors from "../constants/Colors";
import useColorScheme from "../hooks/useColorScheme";

import LoginScreen from "../screens/Login";
import ModalScreen from "../screens/ModalScreen";
import NotFoundScreen from "../screens/NotFoundScreen";
import MainScreen from "../screens/MainScreen";
import Orders from "../screens/Orders";
import {
  RootStackParamList,
  RootTabParamList,
  RootTabScreenProps,
} from "../../types";
import LinkingConfiguration from "./LinkingConfiguration";
import { useAppSelector } from "../redux/root-state.hook";
import Account from "../screens/Account";
import Order from "../screens/Order";
import Track from "../screens/Track";
import Signup from "../screens/Signup";
import PresignUp from "../screens/PresignUp";
import { firebase } from "@react-native-firebase/auth";
import apiKeys from "../config/Keys";

export default function Navigation({
  colorScheme,
}: {
  colorScheme: ColorSchemeName;
}) {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
    >
      <RootNavigator />
    </NavigationContainer>
  );
}

const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigator() {
  const isLogin = useAppSelector((state) => state.user.isLogined);

  const [auth, setAuth] = useState({
    isLoadied: true,
    isAuthenticationReady: false,
    isAuthenticated: false,
  });
  // useEffect(() => {
  //   if (!firebase.apps.length) {
  //     firebase.initializeApp(apiKeys.firebaseConfig);
  //     firebase.auth().onAuthStateChanged((user) => {
  //       console.log("user", user);
  //       setAuth({
  //         isLoadied: true,
  //         isAuthenticationReady: true,
  //         isAuthenticated: !!user,
  //       });
  //     });
  //   }
  // }, []);
  console.log("auth.isAuthenticated", auth.isAuthenticated);

  return (
    <Stack.Navigator>
      {auth.isAuthenticated ? (
        <>
          <Stack.Screen
            name="PresignUp"
            component={PresignUp}
            options={{ headerShown: true, title: "" }}
          />
          <Stack.Screen
            name="Signup"
            component={Signup}
            options={{ headerShown: true, title: "" }}
          />
          <Stack.Screen
            name="LoginScreen"
            component={LoginScreen}
            options={{ headerShown: true, title: "Login" }}
          />
        </>
      ) : (
        <>
          <Stack.Screen
            name="Root"
            component={BottomTabNavigator}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="NotFound"
            component={NotFoundScreen}
            options={{ title: "Oops!" }}
          />
          <Stack.Group screenOptions={{ presentation: "modal" }}>
            <Stack.Screen name="Modal" component={ModalScreen} />
            <Stack.Screen
              name="Order"
              component={Order}
              options={{ headerShown: true, title: "Order" }}
            />
            <Stack.Screen
              name="Track"
              component={Track}
              options={{ headerShown: true, title: "Product Status Track" }}
            />
          </Stack.Group>
        </>
      )}
    </Stack.Navigator>
  );
}

const BottomTab = createBottomTabNavigator<RootTabParamList>();

function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="Main"
      screenOptions={{
        tabBarActiveTintColor: Colors[colorScheme].tint,
      }}
    >
      <BottomTab.Screen
        name="Main"
        component={MainScreen}
        options={({ navigation }: RootTabScreenProps<"Main">) => ({
          title: "Main",
          tabBarIcon: ({ color }) => <TabBarIcon name="home" color={color} />,
        })}
      />
      <BottomTab.Screen
        name="Orders"
        component={Orders}
        options={{
          title: "Orders",
          tabBarIcon: ({ color }) => (
            <TabBarIcon name="briefcase" color={color} size={24} />
          ),
        }}
      />
      <BottomTab.Screen
        name="Account"
        component={Account}
        options={{
          title: "Account",
          tabBarIcon: ({ color }) => <TabBarIcon name="user" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

function TabBarIcon(props: {
  name: React.ComponentProps<typeof FontAwesome>["name"];
  color: string;
}) {
  return <FontAwesome size={30} style={{ marginBottom: -3 }} {...props} />;
}
