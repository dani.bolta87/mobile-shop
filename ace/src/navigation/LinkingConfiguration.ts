/**
 * Learn more about deep linking with React Navigation
 * https://reactnavigation.org/docs/deep-linking
 * https://reactnavigation.org/docs/configuring-links
 */

import { LinkingOptions } from "@react-navigation/native";
import * as Linking from "expo-linking";

import { RootStackParamList } from "../../types";

const linking: LinkingOptions<RootStackParamList> = {
  prefixes: [Linking.makeUrl("/")],
  config: {
    screens: {
      Root: {
        screens: {
          TabOne: {
            screens: {
              MainScreen: "Main",
            },
          },
          TabTwo: {
            screens: {
              Orders: "Orders",
            },
          },
          Account: {
            screens: {
              Orders: "Account",
            },
          },
        },
      },
      Modal: "modal",
      Order: "order",
      NotFound: "*",
    },
  },
};

export default linking;
