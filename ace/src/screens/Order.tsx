import { useNavigation } from "@react-navigation/native";
import React, { useEffect } from "react";
import {
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import { Button, Card, Paragraph, Title } from "react-native-paper";

import { Text, View } from "../components/Themed";
import { useAppDispatch, useAppSelector } from "../redux/root-state.hook";
import { addOrder, IOrder } from "../redux/slices/OrdersSlice";
import uuid from "react-uuid";

export default function Order({ route }) {
  const { item } = route.params;
  const dispatch = useAppDispatch();
  const navigations = useNavigation();

  const data: IOrder = {
    id: item.id,
    orderNumber: uuid(),
    orderDate: new Date().toLocaleString(),
    orderStatus: "Processing",
    title: item.title,
    image: item.image,
  };
  const placeOrder = () => {
    dispatch(addOrder(data));
    alert("Successfully");
    navigations.goBack();
  };
  console.log("data", data);
  return (
    <View style={styles.container}>
      <Card style={styles.card}>
        <Card.Cover source={{ uri: item.image }} style={styles.image} />
        <Card.Content style={styles.content}>
          <ScrollView>
            <Title style={styles.title}>{item.title}</Title>
            <Text style={styles.price}>RM{item.price}</Text>

            <Title style={styles.title}>Description</Title>
            <Paragraph style={styles.paragraph}>{item.description}</Paragraph>
          </ScrollView>
        </Card.Content>
      </Card>

      <Button mode="contained" style={styles.btn} onPress={placeOrder}>
        Place Order
      </Button>
    </View>
  );
}

const screenHeight = Dimensions.get("window").height;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  title: {
    flex: 1,
    fontSize: 16,
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  card: {
    width: "100%",
    height: screenHeight * 0.8,
  },
  image: {
    width: "100%",
    height: screenHeight * 0.4,
  },
  content: {
    height: screenHeight * 0.4,
  },
  paragraph: {},
  price: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#ff0000",
  },
  btn: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    marginHorizontal: 10,
  },
});
