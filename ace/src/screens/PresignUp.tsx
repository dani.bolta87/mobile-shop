import { useNavigation } from "@react-navigation/core";
import * as React from "react";
import { useEffect } from "react";
import { Text, StyleSheet, View, Alert } from "react-native";
import StyledButton from "../components/StyledButton";
import * as firebase from "firebase/app";
import "firebase/firestore";
export default function PresignUp() {
  const navigation = useNavigation();

  useEffect(() => {
    async function getUserInfo() {
      let doc = await firebase
        .firestore()
        .collection("users")
        .doc("x4dwgaOOpy2AWTZaTkSk")
        .get();

      if (!doc.exists) {
        Alert.alert("No user data found!");
      } else {
        let dataObj = doc.data();
        console.log("dataObj", dataObj);
      }
    }
    getUserInfo();
  }, []);

  const goToSignUp = () => {
    navigation.navigate("Signup");
  };
  return (
    <View style={styles.container}>
      <StyledButton
        name="SIGN UP"
        backgroundColor="white"
        color="blue"
        icon=""
        onPress={goToSignUp}
      />
      <StyledButton
        name="LOG IN"
        backgroundColor="blue"
        color="grey"
        icon=""
        onPress={() => navigation.navigate("LoginScreen")}
      />
      {/* <Text>Or</Text> */}
      {/* <StyledButton
        name="Facebook"
        backgroundColor="white"
        color="grey"
        icon="facebook"
        onPress={() => navigation.navigate("Modal", { payload: "facebook" })}
      /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
