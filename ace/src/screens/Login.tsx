import * as React from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  Button,
  Alert,
  ScrollView,
} from "react-native";
import { useForm, Controller } from "react-hook-form";
import Constants from "expo-constants";
import { useState } from "react";
import firebase from "firebase/app";

const LoginScreen = () => {
  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  const onLoginPress = (data: any) => {
    //    firebase.auth().signInWithEmailAndPassword(data.email, data.password)
    //     .then((res) => {
    // console.log(res);
    //     }
  };

  const ResetPassword = (data: any) => {
    // auth()
    //   .sendPasswordResetEmail(data.email)
    //   .then(
    //     () => {
    //       Alert.alert("Password resent link is sent to your email");
    //     },
    //     (error) => {
    //       Alert.alert(error.message);
    //     }
    //   );
  };

  const EMAIL_REGEX =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const {
    register,
    setValue,
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: {
      email: "",
      password: "",
    },
  });
  // const onSubmit = (data: any) => {
  //   console.log(data);
  // };

  const onChange = (arg: { nativeEvent: { text: any } }) => {
    return {
      value: arg.nativeEvent.text,
    };
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.label}>Email</Text>
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={(value) => onChange(value)}
            value={value}
          />
        )}
        name="email"
        rules={{
          required: {
            value: true,
            message: "Email is required",
          },
          pattern: {
            value: EMAIL_REGEX,
            message: "Email is invalid",
          },
        }}
      />
      {errors.email ? (
        <Text style={styles.error}>{errors.email.message}</Text>
      ) : null}
      <Text style={styles.label}>Password</Text>
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={(value) => onChange(value)}
            value={value}
            secureTextEntry={true}
          />
        )}
        name="password"
        rules={{
          required: {
            value: true,
            message: "Password is required",
          },
        }}
      />
      {errors.password && (
        <Text style={styles.error}>{errors.password.message}</Text>
      )}

      <View style={styles.button}>
        <Button title="Log in" onPress={handleSubmit(onLoginPress)} />
      </View>
      <View style={styles.button}>
        <Button
          title="Forget password?"
          // onPress={() => {
          //   reset({
          //     email: "",
          //     password: "",
          //   });
          // }}

          onPress={handleSubmit(ResetPassword)}
        />
      </View>
    </ScrollView>
  );
};
export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,

    paddingTop: Constants.statusBarHeight,
    padding: 8,
    backgroundColor: "#dae3db",
  },
  label: {
    color: "#000a01",
    margin: 20,
    marginLeft: 0,
    fontWeight: "900",
    fontSize: 14,
  },
  button: {
    marginTop: 40,
    color: "white",
    height: 50,
    // backgroundColor: "#ec5990",
    borderRadius: 4,
  },

  input: {
    backgroundColor: "white",
    // borderColor: "none",
    height: 40,
    padding: 10,
    borderRadius: 4,
  },
  error: {
    color: "red",
    margin: 20,
    marginLeft: 0,
    fontSize: 12,
    fontWeight: "700",
  },
});
