import { Dimensions, FlatList, StyleSheet } from "react-native";

import EditScreenInfo from "../components/EditScreenInfo";
import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../../types";
import { ActivityIndicator, Card, Title } from "react-native-paper";
import { useAppDispatch, useAppSelector } from "../redux/root-state.hook";
import React, { useEffect } from "react";
import { fetchProducts } from "../redux/slices/Products";
import { useNavigation } from "@react-navigation/native";

export default function MainScreen({ navigation }: RootTabScreenProps<"Main">) {
  const dispatch = useAppDispatch();
  const { products, loading } = useAppSelector((state) => state.products);
  const navigations = useNavigation();
  useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  const renderOption = ({ item }: any) => (
    <Card
      style={styles.card}
      onPress={() => navigations.navigate<any>("Order", { item: item })}
    >
      <Card.Cover source={{ uri: item.image }} style={styles.image} />
      <Card.Content>
        <Text style={styles.title}>{item.title}</Text>
      </Card.Content>
    </Card>
  );

  if (loading) return <ActivityIndicator />;
  return (
    <View style={styles.container}>
      <FlatList
        showsVerticalScrollIndicator={false}
        legacyImplementation={false}
        data={products}
        renderItem={renderOption}
        keyExtractor={(item, index) => index.toString()}
        ListFooterComponent={<View style={{ height: 100 }} />}
        numColumns={2}
      />
    </View>
  );
}
const screenWidth = Dimensions.get("window").width;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  title: {
    flex: 1,
    fontSize: 12,
    overflow: "hidden",
    flexWrap: "nowrap",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  card: {
    width: screenWidth / 2 - 10,
    margin: 5,
  },
  image: {
    width: screenWidth / 2 - 10,
    aspectRatio: 1 / 1,
  },
});
