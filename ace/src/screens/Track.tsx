import { View, Text, StyleSheet } from "react-native";
import { useState } from "react";
import { Card, Subheading } from "react-native-paper";
import {
  FontAwesome,
  MaterialCommunityIcons,
  MaterialIcons,
} from "@expo/vector-icons";
import { useAppSelector } from "../redux/root-state.hook";
import { IOrder } from "../redux/slices/OrdersSlice";

const Track = ({ route }: any) => {
  const { id } = route.params;
  const { orders } = useAppSelector((state) => state.orders);
  const [text] = useState({
    Pending: "Order is being Prepared to Ship",
    Processing: "Order is being shipped",
    Complete: "Order is complete",
    Cancelled: "Order is cancelled",
  });

  const orderStatus = orders.filter((item: IOrder) => item.id === id)[0]
    .orderStatus;
  console.log("orders", orders);
  return (
    <View>
      <Card style={styles.card}>
        <Card.Content style={styles.content}>
          <Subheading style={styles.title}>{text[orderStatus]}</Subheading>
        </Card.Content>
      </Card>

      <View style={styles.iconsContainer}>
        <View>
          <MaterialCommunityIcons
            name="package-variant"
            size={40}
            color={orderStatus === "Pending" ? "blue" : "grey"}
            style={styles.icon}
          />
          <Text>Processing</Text>
        </View>
        <View>
          <MaterialCommunityIcons
            name="ray-start-arrow"
            size={40}
            color="grey"
          />
          <Text></Text>
        </View>
        <View>
          <MaterialIcons
            name="local-shipping"
            size={40}
            color={orderStatus === "Processing" ? "blue" : "grey"}
            style={styles.icon}
          />
          <Text>Shipping</Text>
        </View>
        <View>
          <MaterialCommunityIcons
            name="ray-start-arrow"
            size={40}
            color="grey"
            style={styles.connectIcon}
          />
          <Text></Text>
        </View>
        <View>
          <FontAwesome
            name="handshake-o"
            size={40}
            color={orderStatus === "Complete" ? "blue" : "grey"}
            style={styles.icon}
          />
          <Text>Delivered</Text>
        </View>

        <View>
          <MaterialCommunityIcons
            name="ray-start-arrow"
            size={40}
            color="grey"
            style={styles.connectIcon}
          />
          <Text></Text>
        </View>
        <View>
          <MaterialCommunityIcons
            name="cancel"
            size={40}
            color={orderStatus === "Cancelled" ? "red" : "grey"}
            style={styles.icon}
          />
          <Text>Canceled</Text>
        </View>
      </View>
    </View>
  );
};

export default Track;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  card: {
    width: "100%",
    backgroundColor: "#000066",
  },
  title: {
    fontSize: 14,
    color: "white",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  content: {},
  icon: {},
  iconsContainer: {
    paddingHorizontal: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    height: 100,
    alignItems: "center",
  },
  connectIcon: {},
});
