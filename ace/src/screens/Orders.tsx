import { useNavigation } from "@react-navigation/native";
import React from "react";
import { FlatList, StyleSheet, Image } from "react-native";
import { Card } from "react-native-paper";

import { Text, View } from "../components/Themed";
import { useAppSelector } from "../redux/root-state.hook";

export default function Orders() {
  const { isLoading, orders } = useAppSelector((state) => state.orders);
  const navigations = useNavigation();
  const goToTrack = (id: number) => {
    navigations.navigate<any>("Track", { id: id });
  };
  const renderOption = ({ item }: any) => {
    return (
      <Card style={styles.card} onPress={() => goToTrack(item.id)}>
        <Card.Title
          title={item.title}
          titleStyle={styles.title}
          subtitle={item.orderDate}
          left={(props) => (
            <Image
              source={{ uri: item.image }}
              style={{ width: 50, height: 50 }}
            />
          )}
        />
      </Card>
    );
  };
  return (
    <View style={styles.container}>
      <FlatList
        showsVerticalScrollIndicator={false}
        legacyImplementation={false}
        data={orders}
        renderItem={renderOption}
        keyExtractor={(item, index) => index.toString()}
        numColumns={2}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  card: {
    width: "100%",
    elevation: 5,
    marginVertical: 5,
  },
  title: {
    fontSize: 10,
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
