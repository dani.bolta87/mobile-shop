import React from "react";
import {
  Text,
  View,
  TextInput,
  Button,
  Alert,
  StyleSheet,
  ScrollView,
} from "react-native";
import { useForm, Controller } from "react-hook-form";
import firebase from "firebase/app";
import { useNavigation } from "@react-navigation/native";

export default function Signup() {
  const navigation = useNavigation();

  const EMAIL_REGEX =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data: any) => console.log(data);

  const onSignup = (data: any) => {
    // firebase
    //   .auth()
    //   .createUserWithEmailAndPassword(data.email, data.password)
    //   .then(
    //     (response) => {
    //       console.log(response);
    //     },
    //     (error) => {
    //       Alert.alert(error.message);
    //     }
    //   );
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.title}>SIGN UP</Text>
      <Text style={styles.label}>First name</Text>
      <Controller
        control={control}
        rules={{
          required: true,
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
        name="firstName"
        defaultValue=""
      />
      {errors.firstName && <Text style={styles.error}>This is required.</Text>}

      <Text style={styles.label}>Last name</Text>

      <Controller
        control={control}
        rules={{
          maxLength: 100,
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
        name="lastName"
        defaultValue=""
      />
      {errors.lastName && <Text style={styles.error}>This is required.</Text>}
      <Text style={styles.label}>Email</Text>
      <Controller
        control={control}
        rules={{
          required: {
            value: true,
            message: "Email is required",
          },
          pattern: {
            value: EMAIL_REGEX,
            message: "Email is invalid",
          },
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
        name="email"
        defaultValue=""
      />

      {errors.email && <Text style={styles.error}>{errors.email.message}</Text>}

      <Text style={styles.label}>Password</Text>
      <Controller
        control={control}
        rules={{
          required: true,
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
            secureTextEntry={true}
          />
        )}
        name="password"
        defaultValue=""
      />
      {errors.password && <Text style={styles.label}>This is required.</Text>}

      <Text style={styles.label}>Mobile</Text>
      <Controller
        control={control}
        rules={{
          required: true,
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
            keyboardType={"phone-pad"}
          />
        )}
        name="mobile"
        defaultValue=""
      />
      {errors.mobile && <Text style={styles.label}>This is required.</Text>}

      <View style={styles.button}>
        <Button
          // color
          title="Submit"
          onPress={handleSubmit(onSignup)}
        />
      </View>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  keyboard: {},
  inner: {
    padding: 24,
    flex: 1,
    justifyContent: "space-around",
  },
  label: {
    color: "black",
    marginVertical: 10,
    marginLeft: 0,
  },
  button: {
    color: "white",
    height: 50,
    marginTop: 20,
    borderRadius: 4,
  },
  container: {
    backgroundColor: "#cfcfcf",
    paddingHorizontal: 15,
  },
  input: {
    backgroundColor: "white",
    borderColor: "white",
    height: 40,
    padding: 10,
    borderRadius: 4,
    fontFamily: "space-mono",
  },
  title: {
    color: "white",
    marginVertical: 40,
    alignSelf: "center",
    fontSize: 18,
    fontFamily: "space-mono",
  },
  error: {
    color: "red",
    margin: 20,
    marginLeft: 0,
    fontSize: 12,
    fontWeight: "700",
  },
});
