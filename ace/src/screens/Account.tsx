import { StyleSheet, TouchableOpacity } from "react-native";

import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../../types";
import { useAppDispatch, useAppSelector } from "../redux/root-state.hook";
import { Avatar } from "react-native-paper";
import React from "react";
import { logout } from "../redux/slices/UserSlice";
export default function Account({ navigation }: RootTabScreenProps<"Account">) {
  const dispatch = useAppDispatch();
  const { email, firstName, lastName } = useAppSelector(
    (state) => state.user.user
  );
  const logoutHandler = () => {
    dispatch(logout());
  };
  return (
    <View style={styles.container}>
      <View style={{}}>
        <Avatar.Image source={require("../assets/images/icon.png")} />
        <Text>
          {firstName} {lastName}
        </Text>
      </View>
      <TouchableOpacity style={styles.logout} onPress={logoutHandler}>
        <Text style={styles.logoutTxt}>Log out</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  logout: {
    position: "absolute",
    top: 10,
    right: 20,
  },
  logoutTxt: {
    fontWeight: "bold",
  },
});
