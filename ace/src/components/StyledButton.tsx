import { FontAwesome } from "@expo/vector-icons";
import React from "react";
import { Text, TouchableOpacity, useColorScheme } from "react-native";
import Colors from "../constants/Colors";
import window from "./../constants/Layout";

const { height, width } = window.window;
const StyledButton = ({ name, backgroundColor, color, icon, onPress }) => {
  const colorScheme = useColorScheme();
  return (
    <TouchableOpacity
      style={{
        width: width - 30,
        height: 50,
        backgroundColor: backgroundColor,
        elevation: 4,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 7,
        marginVertical: 10,
        flexDirection: "row",
      }}
      onPress={onPress}
    >
      <FontAwesome
        name={icon}
        size={25}
        color={Colors[colorScheme].text}
        style={{ marginRight: 15 }}
      />
      <Text
        style={{
          fontSize: 14,
          fontWeight: "700",
          color: color,
        }}
      >
        {name}
      </Text>
    </TouchableOpacity>
  );
};

export default StyledButton;
