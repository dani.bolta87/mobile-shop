import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface IUser {
  id: number;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
}
interface userState {
  user: IUser;
  isLogined: boolean;
}
const initialState: userState = {
  isLogined: false,
  user: {
    id: 0,
    email: "",
    firstName: "",
    password: "",
    lastName: "",
  },
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<IUser>) => {
      state.user = action.payload;
    },
    login: (state: any, action: PayloadAction<IUser>) => {
      state.user = action.payload;
      state.isLogined = true;
    },
    logout: (state: any) => {
      state.user = initialState.user;
      state.isLogined = false;
    },
  },
});

export const { setUser, login, logout } = userSlice.actions;

export default userSlice.reducer;
