import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { createAsyncThunk } from "@reduxjs/toolkit";
export interface IProduct {
  id: number;
  title: string;
  url: string;
}
interface userState {
  products: IProduct[];
  loading: boolean;
}

const initialState: userState = {
  loading: false,
  products: [],
};

const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    setProducts: (state, action: PayloadAction<IProduct[]>) => {
      state.products = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchProducts.pending, (state: { loading: boolean }) => {
      state.loading = true;
    });
    builder.addCase(
      fetchProducts.fulfilled,
      (
        state: { loading: boolean; products: any[] },
        action: { payload: any }
      ) => {
        state.loading = false;
        state.products = action.payload;
      }
    );
    builder.addCase(fetchProducts.rejected, (state: { loading: boolean }) => {
      state.loading = false;
    });
  },
});

export const { setProducts } = productsSlice.actions;

export default productsSlice.reducer;

export const fetchProducts = createAsyncThunk(
  "products/fetchProducts",
  async (): Promise<IProduct[]> => {
    const response = await fetch("https://fakestoreapi.com/products");
    const data = await response.json();
    return data;
  }
);
