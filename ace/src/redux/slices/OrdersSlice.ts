import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface IOrder {
  id: number;
  orderNumber: string;
  orderDate: string;
  orderStatus: "Pending" | "Processing" | "Complete" | "Cancelled";
  title: string;
  image: string;
}

interface IOrderSState {
  orders: IOrder[];
  isLoading: boolean;
}

const initialState: IOrderSState = {
  isLoading: false,
  orders: [],
};

const productsSlice = createSlice({
  name: "orders",
  initialState,
  reducers: {
    setOrders: (state, action: PayloadAction<any>) => {
      state.orders = action.payload;
    },
    addOrder: (state, action: PayloadAction<any>) => {
      state.orders.push(action.payload);
    },
  },
});
export const { setOrders, addOrder } = productsSlice.actions;

export default productsSlice.reducer;
