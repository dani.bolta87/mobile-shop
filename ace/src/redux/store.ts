import {
  combineReducers,
  configureStore,
  createSerializableStateInvariantMiddleware,
  isPlainObject,
} from "@reduxjs/toolkit";
import userReducer from "./slices/UserSlice";
import productsReducer from "./slices/Products";
import ordersReducer from "./slices/OrdersSlice";

function isPlain(val: any): boolean {
  return (
    typeof val === "undefined" ||
    val === null ||
    typeof val === "string" ||
    typeof val === "boolean" ||
    typeof val === "number" ||
    Array.isArray(val) ||
    isPlainObject(val)
  );
}

const isSerializable = (value: any) => isPlain(value);

const getEntries = (value: any) => Object.entries(value);

const serializableMiddleware = createSerializableStateInvariantMiddleware({
  isSerializable,
  getEntries,
});
const reducer = combineReducers({
  user: userReducer,
  products: productsReducer,
  orders: ordersReducer,
});

const store = configureStore({
  reducer,
  // middleware: getDefaultMiddleware =>
  //   getDefaultMiddleware({
  //     serializableCheck: false,
  //   }),
  // middleware: [serializableMiddleware]
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;
